package udp_server;


import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 *
 * @author GT303-19
 */
public class UDPServer {

    public static void main(String[] args) throws Exception {
        BufferedReader inFromUser =
         new BufferedReader(new InputStreamReader(System.in));
        Scanner in = new Scanner(System.in);
        System.out.println("Server Start");
        System.out.println("Who's the Client?");
        String user = in.nextLine();
        DatagramSocket serverSocket = new DatagramSocket(5000);
            byte[] receiveData = new byte[1024];
            byte[] sendData = new byte[1024];
            int i = 0;
            while(i <= 7)
               {
                  DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                  serverSocket.receive(receivePacket);
                  String sentence = new String( receivePacket.getData());
                  System.out.println(user + ":" + sentence);
                  InetAddress IPAddress = receivePacket.getAddress();
                  int port = receivePacket.getPort();
                  String backSentence = inFromUser.readLine();
                  sendData = backSentence.getBytes();
                  DatagramPacket sendPacket =
                  new DatagramPacket(sendData, sendData.length, IPAddress, port);
                  serverSocket.send(sendPacket);
                  i++;
               }
    } 
}